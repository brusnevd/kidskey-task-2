let rectangle, message, text = ["go", "go to", "go on", "go for"],
    // answersText = ["home", "bed", "a run", "shopping", "school", "a swim", "dancing", "church", "a date", "a cruise", "vacation", "a walk", ],
    answsText = new Map(
        [
            ["home", 0],
            ["bed", 1],
            ["a run", 3],
            ["shopping", 0],
            ["school", 1],
            ["a swim", 3],
            ["dancing", 0],
            ["church", 1],
            ["a date", 2],
            ["a cruise", 2],
            ["vacation", 2],
            ["a walk", 3]
        ]
    ),
    answersText = {
        "home": 0,
        "bed": 1,
        "a run": 3,
        "shopping": 0,
        "school": 1,
        "a swim": 3,
        "dancing": 0,
        "church": 1,
        "a date": 2,
        "a cruise": 2,
        "vacation": 2,
        "a walk": 3,
    },
    counts = {
        '0': 0,
        '1': 0,
        '2': 0,
        '3': 0,
    },
    answRect, answMessage,
    containerForAnswers,
    containers = [],
    answerRects = [],
    container,
    oldPosX, endPosY,
    i, j,
    background;

let app = new PIXI.Application({
    width: 400,
    height: 500
});

let style = new PIXI.TextStyle({
    fontSize: 20
});

let answStyle = new PIXI.TextStyle({
    fontSize: 15
});

function Header(text) {
    let container = new PIXI.Container();

    for (i = 0; i < 4; i++) {
        rectangle = new PIXI.Graphics();
        i % 2 === 0 ? rectangle.beginFill(0x7DF3F3) : rectangle.beginFill(0x7DC4F3);
        rectangle.drawRect(100 * i, 0, 100, 100);
        rectangle.endFill();
        message = new PIXI.Text(text[i], style);
        message.position.set((100 * i) + (100 - message.width) / 2, 35);
        container.addChild(rectangle, message);
    }

    return container;
}

function Main() {
    let container = new PIXI.Container();

    for (i = 0; i < 4; i++) {
        let answerArea = new PIXI.Container(),
            suchRect;

        suchRect = new PIXI.Container();
        suchRect.name = `rect${i}`;
        background = new PIXI.Graphics();
        i % 2 === 0 ? background.beginFill(0xDEDEDE) : background.beginFill(0xB2B2B2);
        background.drawRect(100 * i, 100, 100, 300);
        background.endFill();

        suchRect.interactive = true;

        suchRect.addChild(background);
        answerArea.addChild(suchRect);

        containers.push(suchRect);

        container.addChild(answerArea);
    }

    return container;
}

function Footer(answs) {
    let container = new PIXI.Container(),
        suchRect;
    for (i = 0; i < Object.keys(answs).length; i++) {
        background = new PIXI.Graphics();
        background.beginFill(0x49F3F3)
        background.drawRect(100 * i, 400, 100, 100);
        background.endFill();
        container.addChild(background);
    }
    i = 0;

    for (i = 0; i < Object.keys(answs).length; i++) {
        answRect = new PIXI.Graphics();
        answRect.beginFill(0xffffff)
        answRect.drawRect(0, 0, 80, 80);
        answRect.endFill();
        answMessage = new PIXI.Text(Object.entries(answersText)[i][0], answStyle);
        suchRect = new PIXI.Container();
        suchRect.addChild(answRect, answMessage);
        suchRect.position.set(100 * i + 50, 4 * 100 + 50);
        answMessage.position.set(40 - answMessage.width / 2, 40 - answMessage.height / 2);
        suchRect.addChild(answRect);
        suchRect.addChild(answMessage);
        suchRect.pivot.set(40, 40);

        suchRect.name = Object.entries(answersText)[i].join(" ");

        console.log(suchRect.zIndex);

        // suchRect.zIndex = 1;

        console.log(suchRect.zIndex);

        suchRect.interactive = true;
        suchRect.buttonMode = true;
        suchRect
            .on("pointerdown", onDragStart)
            .on("pointerup", onDragEnd)
            .on("pointerupoutside", onDragEnd)
            .on("pointermove", onDragMove);

        answerRects.sortableChildren = true;

        answerRects.push(suchRect);
        container.addChild(background, suchRect);
    }

    console.log(answerRects)

    return container;
}

document.body.appendChild(app.view);
app.renderer.backgroundColor = 0xE4E4E4;

app.stage.addChild(Header(text), Main(), Footer(answersText));

function onDragStart(event) {
    oldPosX = this.x;
    oldPosY = this.y;
    this.data = event.data;
    this.event = event;
    this.dragging = true;
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    this.data = null;
    let y, name = this.name[this.name.length - 1],
        key = this.name.slice(0, this.name.length - 2),
        obj, flag = 0,
        endRect;

    if (name == 0) {
        if (containers[0].children[0].containsPoint(this.position)) {
            y = 150, i = 0;
            if (counts['0'] == 1) {
                y = 250;
            } else if (counts['0'] == 2) {
                y = 350;
            }
            counts['0'] += 1;
            this.x = 50;
            this.y = y;
            this.interactive = false;

            flag = 0;

            if (Object.keys(answersText).length > 4) {
                for (i = 0; i < Object.keys(answersText).length; i++) {
                    if (Object.entries(answersText)[i][0] == key) {
                        for (j = i + 1; j < Object.keys(answersText).length; j++) {
                            answerRects[j].x = 100 * j - 50;
                        }
                        console.log("\n")
                        delete answersText[key];
                        answerRects.splice(i, 1);
                        console.log(answerRects)
                        break;
                    }
                }
            }

            return
        }
    }
    if (name == 1) {
        if (containers[1].children[0].containsPoint(this.position)) {
            y = 150, i = 0;
            if (counts['1'] == 1) {
                y = 250;
            } else if (counts['1'] == 2) {
                y = 350;
            }
            counts['1'] += 1;
            this.x = 150;
            this.y = y;
            this.interactive = false;

            if (Object.keys(answersText).length > 4) {
                for (i = 0; i < Object.keys(answersText).length; i++) {
                    if (Object.entries(answersText)[i][0] == key) {
                        for (j = i + 1; j < Object.keys(answersText).length; j++) {
                            answerRects[j].x = 100 * j - 50;
                        }
                        delete answersText[key];
                        answerRects.splice(i, 1);
                        console.log(answerRects)
                        console.log("\n")
                        break;
                    }
                }
            }

            return
        }
    }
    if (name == 2) {
        if (containers[2].children[0].containsPoint(this.position)) {
            y = 150;
            if (counts['2'] == 1) {
                y = 250;
            } else if (counts['2'] == 2) {
                y = 350;
            }
            counts['2'] += 1;
            this.x = 250;
            this.y = y;
            this.interactive = false;

            if (Object.keys(answersText).length > 4) {
                for (i = 0; i < Object.keys(answersText).length; i++) {
                    if (Object.entries(answersText)[i][0] == key) {
                        for (j = i + 1; j < Object.keys(answersText).length; j++) {
                            answerRects[j].x = 100 * j - 50;
                        }
                        console.log("\n")
                        delete answersText[key];
                        answerRects.splice(i, 1);
                        console.log(answerRects)
                        break;
                    }
                }
            }

            return
        }
    }
    if (name == 3) {
        if (containers[3].children[0].containsPoint(this.position)) {
            y = 150;
            if (counts['3'] == 1) {
                y = 250;
            } else if (counts['3'] == 2) {
                y = 350;
            }
            counts['3'] += 1;
            this.x = 350;
            this.y = y;
            this.interactive = false;

            if (Object.keys(answersText).length > 4) {
                for (i = 0; i < Object.keys(answersText).length; i++) {
                    if (Object.entries(answersText)[i][0] == key) {
                        for (j = i + 1; j < Object.keys(answersText).length; j++) {
                            answerRects[j].x = 100 * j - 50;
                        }

                        console.log("\n")
                        delete answersText[key];
                        answerRects.splice(i, 1);
                        console.log(answerRects)
                        break;
                    }
                }
            }
            return
        }
    }

    this.x = oldPosX;
    this.y = oldPosY;
}

function onDragMove() {
    if (this.dragging) {
        const newPos = this.data.getLocalPosition(this.parent);
        this.x = newPos.x;
        this.y = newPos.y;
    }
}

console.log(answerRects)